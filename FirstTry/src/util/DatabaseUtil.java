package util;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import model.Animal;
import model.Personal;
import model.Programare;

/**
 *This class will help us to handle the database
 */
public class DatabaseUtil {
	public static EntityManagerFactory entityManagerFactory;
	public static EntityManager entityManager;
	
	/**
	 * Initializes the entityManager and entityManagerFactory
	 * */
	public void setUP() throws Exception {
		entityManagerFactory = Persistence.createEntityManagerFactory("FirstTry");
		entityManager = entityManagerFactory.createEntityManager();
	}
	
	/**
	 * Starts the data transaction
	 * */
	public void startTransaction() {
		entityManager.getTransaction().begin();
	}
	
	/**
	 * The function inserts a new animal in the database
	 * @param animal is the new animal you want to insert in the database
	 * */
	public void saveAnimal(Animal animal) {
		entityManager.persist(animal);
	}
	
	/**
	 * Inserts a new doctor in the database
	 * @param pers is the new doctor you want to insert in the DB
	 * */
	public void savePersonal(Personal pers) {
		entityManager.persist(pers);
	}
	
	/**
	 * Inserts a new appointment in the database
	 * @param prog is the new appointment you want to insert in DB
	 * */
	public void saveAppointment(Programare prog) {
		entityManager.persist(prog);
	}
	
	
	/**
	 * Saves the changes that you had done in database
	 * */
	public void commitTransaction() {
		entityManager.getTransaction().commit();
	}
	
	/**
	 * Closes the connections with the database
	 * */
	public void closeEntityManager() {
			entityManager.close();
	}
	
	/**
	 * Prints all animals from database
	 * */
	public void printAllAnimalsFromDB() {
		Date date = new Date();
		List<Animal> results = entityManager.createNativeQuery("Select * from petshop.animal", Animal.class).getResultList();
		
		results.forEach((Animal animal)->{System.out.println("Animal id :"+animal.getIdAnimal()+" name: "+animal.getName() +" owner: "+animal.getOwner()+" weight: "+ animal.getWeight()
		+" birthdate: "+animal.getBirthdate()+" age: "+ (date.getTime() - animal.getBirthdate().getTime())/86400000/365);
		});
		
		/*
		for (Animal animal : results) {
				System.out.println("Animal id :"+animal.getIdAnimal()+" name: "+animal.getName() +" owner: "+animal.getOwner()+" weight: "+ animal.getWeight()
						+" birthdate: "+animal.getBirthdate()+" age: "+ (date.getTime() - animal.getBirthdate().getTime())/86400000/365);
		}
		*/
	}
	
	/**
	 * Prints all doctors from database
	 * */
	public void printAllDoctorsFromDB() {
		List<Personal> results = entityManager.createNativeQuery("Select * from petshop.personal", Personal.class).getResultList();
		results.forEach((Personal pers)->{System.out.println("Personal id :"+ pers.getIdpersonal()+" name: "+pers.getNume());});
		
		/*
		for (Personal pers : results) {
				System.out.println("Personal id :"+ pers.getIdpersonal()+" name: "+pers.getNume());
		}
		*/
	}
	
	
	
	/**
	 * Generic function that find anything by id
	 */
	public   <T extends Serializable> T  find(int id) {
		
		
		T temporary = null;
		Animal a = new Animal();
		Personal p = new Personal();
		Programare pr =new Programare();
		
		
		if(temporary.getClass().equals(a.getClass())) {
			return (T) this.animalFind(id);
		}
		
		if(temporary.getClass().equals(p.getClass())) {
			return (T) this.doctorFind(id);
		}
		if(temporary.getClass().equals(pr.getClass())) {
			return (T) this.appointmentFind(id);
		}
		return null;
			
	}
	
	
	
	
	
	/**
	 * Prints all appointments form database
	 * */
	public void printAllAppointmentsFromDB() {
		List<Programare> results = entityManager.createNativeQuery("Select * from petshop.programare", Programare.class).getResultList();
		
		results.forEach((Programare prog)->{System.out.println("Prog id: "+prog.getIdprogramare()+" animal: "+prog.getIdanimal()+" personal: "+prog.getIdpersonal()+" time: "+prog.getDate());
		});
		
		/*
		for (Programare prog : results) {
				System.out.println("Prog id: "+prog.getIdprogramare()+" animal: "+prog.getIdanimal()+" personal: "+prog.getIdpersonal()+" time: "+prog.getDate());
		}
		*/
	}
	
	/**
	 * Returns an animal by the index
	 * @param index :id of the animal you want to find
	 * */
	public final Animal animalFind(int index) {
		List<Animal> results = entityManager.createNativeQuery("Select * from petshop.animal where idAnimal ="+index, Animal.class).getResultList();
		return results.get(0);
	}
	
	/**
	 * Updates the name of an animal
	 * The animal will be found by id
	 * @param index : id of the animal you want to update
	 * @param newName : the name you give to the animal
	 * */
	public void updateAnimal(int index, String newName) {
		  Animal anim= (Animal)entityManager.find(Animal.class ,index);
		  anim.setName(newName);
		 // entityManager.getTransaction().commit();
	}
	
	/**
	 * Delete an animal by id
	 * @param index : id of the animal you want to delete from database
	 * */
	public void deleteAnimal(int index) {
		List<Programare> results = entityManager.createNativeQuery("Select * from petshop.programare where idanimal = "+index, Programare.class).getResultList();
		for (Programare prog : results) {
			entityManager.remove(prog);
		}
		  Animal anim= (Animal)entityManager.find(Animal.class ,index);
	//	Animal anim = this.find(index);
		  entityManager.remove(anim);
		 // entityManager.getTransaction().commit();
	}
	
	
	
	
	/*
	 * Find a doctor by id
	 * @param index : id of the doctor you want to find
	 * */
	public final Personal doctorFind(int index) {
		List<Personal> results = entityManager.createNativeQuery("Select * from petshop.personal where idpersonal ="+index, Personal.class).getResultList();
		return results.get(0);
	}
	
	/*
	 * Change name of a doctor
	 * @param index : the if of doctor you want to change
	 * @param newName: the name you want to give to the doctor
	 * */
	public void updateDoctor(int index, String newName) {
		  Personal pers= (Personal)entityManager.find(Personal.class ,index);
		  pers.setNume(newName);
		 // entityManager.getTransaction().commit();
	}
	
	/*
	 * Delete a doctor from database
	 * The doctor will be found by id
	 * @param index : id of the doctor you want to delete
	 * */
	public void deleteDoctor(int index) {
		List<Programare> results = entityManager.createNativeQuery("Select * from petshop.programare where idpersonal = "+index, Programare.class).getResultList();
		for (Programare prog : results) {
			entityManager.remove(prog);
		}
		  Personal pers= (Personal)entityManager.find(Personal.class ,index);
		  entityManager.remove(pers);
		 // entityManager.getTransaction().commit();
	}
	
	/*
	 * Find an appointment by id
	 * @param index : id of the appointment you want to see
	 * */
	public final Programare appointmentFind(int index) {
		List<Programare> results = entityManager.createNativeQuery("Select * from petshop.programare where idprogramare ="+index,Programare.class).getResultList();
		return results.get(0);
	}
	
	
	/*
	 * Delete an appointment found by id
	 * @param index : id of the appointment you want to delete
	 * */
	public void deleteAppointment(int index) {
		  Programare prog= (Programare)entityManager.find(Programare.class ,index);
		  entityManager.remove(prog);
		//  entityManager.getTransaction().commit();
	}
	
	
	/*
	 * Update the animal of an appointment
	 * @param index : id of the record you want to change
	 * @param animalIndex: id of the new animal in the appointment
	 * */
	public void updateAppointmentAnimal(int index, int animalIndex) {
		  Programare prog= (Programare)entityManager.find(Programare.class ,index);
		  prog.setIdanimal(animalIndex);
		 // entityManager.getTransaction().commit();
	}
	
	/*
	 * Update the doctor of an appointment
	 * @param index : id of the record you want to change
	 * @param doctorIndex : id of the new doctor in the appointment
	 * */
	public void updateAppointmentDoctor(int index, int doctorIndex) {
		  Programare prog= (Programare)entityManager.find(Programare.class ,index);
		  prog.setIdpersonal(doctorIndex);;
		//  entityManager.getTransaction().commit();
	}
	
	/**
	 * Update the date of an appointment
	 * @param index: id of the appointment you want to change
	 * @param date: the new date in the appointment
	 * */
	public void updateAppointmentDate(int index, Date date) {
		  Programare prog= (Programare)entityManager.find(Programare.class ,index);
		  prog.setDate(date);
		//  entityManager.getTransaction().commit();
	}

	
	/**
	 * Print all appointments from database sorted by date
	 * It will also print the name of the animal and the name of the doctor
	 * */
	public void sortPrint() {
		List<Programare> results = entityManager.createNativeQuery("Select * from petshop.programare order by date", Programare.class).getResultList();
		
		results.forEach((Programare prog)->{System.out.println("Prog id: "+prog.getIdprogramare()+" animal: "+ this.animalFind(prog.getIdanimal()).getName()+" personal: "+this.doctorFind(prog.getIdpersonal()).getNume()+" time: "+prog.getDate());});
		
		/*
		for (Programare prog : results) {
				System.out.println("Prog id: "+prog.getIdprogramare()+" animal: "+ this.animalFind(prog.getIdanimal()).getName()+" personal: "+this.doctorFind(prog.getIdpersonal()).getNume()+" time: "+prog.getDate());
		}
		*/
	}
	
	
	public List<Programare> appointmentList(){
		List<Programare> results = entityManager.createNativeQuery("Select * from petshop.programare order by date", Programare.class).getResultList();
		return results;
		
	}
	
	
}
