package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import model.Animal;
import model.Personal;
import model.Programare;

public class UnitTests {

	Animal animalForTest = new Animal();
	
	
	@Test
	public void testAnimalSetterForId() {
		animalForTest.setIdAnimal(10);
		assertTrue(animalForTest.getIdAnimal()==10);
	}
	
	@Test
	public void testAnimalSetterForName() {
		animalForTest.setName("idk");;
		assertTrue(animalForTest.getName()=="idk");
	}
	
	@Test
	public void testAnimalSetterForOwner() {
		animalForTest.setOwner("John");
		assertTrue(animalForTest.getOwner()=="John");
	}
	
	@Test
	public void testAnimalSetterForWeight() {
		animalForTest.setWeight(15);
		assertTrue(animalForTest.getWeight() == 15);
	}
	
	Personal pers = new Personal();
	
	@Test
	public void testidPersonal() {
		pers.setIdpersonal(5);
		assertTrue(pers.getIdpersonal()==5);
	}
	
	@Test
	public void testNamePersonal() {
		pers.setNume("Yuri");
		assertTrue(pers.getNume() == "Yuri");
	}
	
	Programare prog = new Programare();
	
	@Test
	public void testProgId() {
		prog.setIdprogramare(5);
		assertTrue(prog.getIdprogramare()==5);
	}
	
	@Test
	public void testProgIdPers() {
		prog.setIdpersonal(7);
		assertTrue(prog.getIdpersonal()==7);
	}
	
	@Test
	public void testProgIdAnimal() {
		prog.setIdanimal(9);
		assertTrue(prog.getIdanimal() == 9);
	}
}
