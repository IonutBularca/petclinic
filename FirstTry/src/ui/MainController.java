package ui;

import java.awt.Desktop.Action;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.Date;
import java.util.List;

import java.util.ResourceBundle;
import java.util.StringTokenizer;

import javax.imageio.ImageIO;
import javax.swing.event.ChangeListener;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import model.Animal;
import model.Personal;
import model.Programare;
import util.DatabaseUtil;

public class MainController implements Initializable{

	@FXML
	private ListView<String> listView;
	
	@FXML
	private TextField animalName;
	
	@FXML
	private TextField birthDate;
	@FXML
	private TextField weight;
	@FXML
	private TextField owner;
	@FXML
	private TextField age;
	@FXML
	private TextField doc;
	
	@FXML
	private Button done;
	@FXML
	private Button cancel;
	@FXML
	private ImageView photo;
	
	
	public void done_() throws Exception {
		if(listView.getSelectionModel().getSelectedIndex()!=-1) {
			DatabaseUtil db = new DatabaseUtil();
			db.setUP();
			db.startTransaction();
			int temp1 = listView.getSelectionModel().getSelectedIndex();
			StringTokenizer temp = new StringTokenizer(listView.getSelectionModel().getSelectedItem());
			String id = temp.nextToken("-");
			int appointmentId = Integer.parseInt(id);
			db.deleteAppointment(appointmentId);
			listView.getItems().remove(temp1);
			listView.refresh();
			db.closeEntityManager();
		}
	}
	public void cancel_() throws Exception {
		if(listView.getSelectionModel().getSelectedIndex()!=-1) {
			DatabaseUtil db = new DatabaseUtil();
			db.setUP();
			db.startTransaction();
			int temp1 = listView.getSelectionModel().getSelectedIndex();
			StringTokenizer temp = new StringTokenizer(listView.getSelectionModel().getSelectedItem());
			String id = temp.nextToken("-");
			int appointmentId = Integer.parseInt(id);
			db.deleteAppointment(appointmentId);
			listView.getItems().remove(temp1);
			listView.refresh();
			db.closeEntityManager();
		}
	}
	
	
	public void populateMainListView() throws Exception {
		DatabaseUtil db = new DatabaseUtil();
		db.setUP();
		db.startTransaction();
		List<Programare> progList = (List<Programare>)db.appointmentList();
		ObservableList<String> pr = getAppId(progList);
		listView.setItems(pr);
		listView.refresh();
		db.closeEntityManager();
	}
	

	
	public ObservableList<String> getAppId(List<Programare> appointments){
		ObservableList<String> ids = FXCollections.observableArrayList();
		for(Programare pr : appointments) {
			ids.add(pr.getIdprogramare()+"-"+pr.getDate().getDay()+"."+pr.getDate().getMonth()+"."+pr.getDate().getYear()+"-"+pr.getDate().getHours()+":"+pr.getDate().getMinutes());
		}
		return ids;
	}
	
	
	
	
	public void select() throws Exception {
		StringTokenizer temp = new StringTokenizer(listView.getSelectionModel().getSelectedItem());
		String id = temp.nextToken("-");
		//System.out.println(id);
		int appointmentId = Integer.parseInt(id);
		DatabaseUtil db = new DatabaseUtil();
		db.setUP();
		db.startTransaction();
		
		Animal anim = db.animalFind(db.appointmentFind(appointmentId).getIdanimal());
		Personal doctor = db.doctorFind(db.appointmentFind(appointmentId).getIdanimal());
		
		//System.out.println(anim.getName()+" "+doctor.getNume());
		
		animalName.setText("Animal's name: "+anim.getName());
		
		weight.setText("Animal's weight: "+anim.getWeight());
		
		birthDate.setText("Animal's birthdate: "+anim.getBirthdate().getDay()+"."+anim.getBirthdate().getMonth()+"."+anim.getBirthdate().getYear());
		
		owner.setText("Animal's owner: "+anim.getOwner());
		
		doc.setText("Doctor: "+doctor.getNume());
		
		
		age.setText("Age: "+ (new Date().getTime() - anim.getBirthdate().getTime())/86400000/365);
		
		
	/*	
		
		try {
            String dirName = "C:\\Users\\Ionut\\eclipse-workspace\\FirstTry\\src\\ui";
            BufferedImage imag = ImageIO.read(new ByteArrayInputStream(anim.getPicture()));
            ImageIO.write(imag, "jpeg", new File(dirName , "img.jpeg"));
        } catch (Exception e) {
            System.out.println(e);
        }
        try {
            FileInputStream inputStream = new FileInputStream("C:\\Users\\Ionut\\eclipse-workspace\\FirstTry\\src\\ui\\img.jpeg");
            Image i = new Image(inputStream);
           photo.setImage(i);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
		*/
		
		
	
		FileInputStream inputStream = new FileInputStream("C:\\Users\\Ionut\\eclipse-workspace\\FirstTry\\src\\ui\\testDog.jpg");
        Image imagaga = new Image(inputStream);
		
		photo.setImage(imagaga);
		
	        
	
		db.closeEntityManager();
		
	}
	
	
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {
			populateMainListView();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
	
	}
	
}
