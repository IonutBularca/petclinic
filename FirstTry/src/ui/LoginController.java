package ui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Scanner;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;


public class LoginController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button login;

    @FXML
    private TextField output;

    @FXML
    private TextField password;

    @FXML
    private TextField user;


    @FXML
    void loginFunc(ActionEvent event) {
    	try {
			Socket socket = new Socket("localhost", 5000);
			BufferedReader  echoes= new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintWriter stringToEcho = new PrintWriter(socket.getOutputStream(),true);
			String echoString;
			String response ="";
			Scanner sc = new Scanner(System.in);
			
				echoString = user.getText() + '-' + password.getText();
				stringToEcho.println(echoString);
				if(!echoString.equals("exit")) {
					response = echoes.readLine();
				}
				
				output.setText(response);
				
				if(response.equals("Accepted")) {
					Stage primaryStage = (Stage) login.getScene().getWindow();
					 try {
							BorderPane root = (BorderPane)  FXMLLoader.load(getClass().getResource("/ui/MainView.fxml"));
							Scene scene = new Scene(root,800,800);
							primaryStage.setScene(scene);
							primaryStage.show();
						 } catch (Exception e) {
							e.printStackTrace();
						}
					
				}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	
    }

    @FXML
    void initialize() {
        assert login != null : "fx:id=\"login\" was not injected: check your FXML file 'Login.fxml'.";
        assert output != null : "fx:id=\"output\" was not injected: check your FXML file 'Login.fxml'.";
        assert password != null : "fx:id=\"password\" was not injected: check your FXML file 'Login.fxml'.";
        assert user != null : "fx:id=\"user\" was not injected: check your FXML file 'Login.fxml'.";


    }

}
