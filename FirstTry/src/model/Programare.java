package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the programare database table.
 * 
 */
@Entity
@NamedQuery(name="Programare.findAll", query="SELECT p FROM Programare p")
public class Programare implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idprogramare;

	@Temporal(TemporalType.TIMESTAMP)
	private Date date;

	private int idanimal;

	private int idpersonal;

	public Programare() {
	}

	/*
	 * Returns the id of an appointment
	 * */
	public int getIdprogramare() {
		return this.idprogramare;
	}

	/*
	 * Set the id of an appointment
	 * @param idprogramare is the new id
	 * */
	public void setIdprogramare(int idprogramare) {
		this.idprogramare = idprogramare;
	}

	/*
	 * Returns the date of an appointment
	 * */
	public Date getDate() {
		return this.date;
	}

	/*
	 * Set the date of an appointment
	 * @param date is the new date of the appointment
	 * */
	public void setDate(Date date) {
		this.date = date;
	}

	/*
	 * Returns the if of the animal in the appointment
	 * */
	public int getIdanimal() {
		return this.idanimal;
	}

	/*
	 * Set the id of the animal in appointment
	 * @param idanimal is the new id
	 * */
	public void setIdanimal(int idanimal) {
		this.idanimal = idanimal;
	}

	/*
	 * Returns the if of the doctor in the appointment
	 * */
	public int getIdpersonal() {
		return this.idpersonal;
	}

	/*
	 * Set the id of the doctor in the appointment
	 * @param idpersonal is the new id
	 * */
	public void setIdpersonal(int idpersonal) {
		this.idpersonal = idpersonal;
	}

}