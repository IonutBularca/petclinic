package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the personal database table.
 * 
 */
@Entity
@NamedQuery(name="Personal.findAll", query="SELECT p FROM Personal p")
public class Personal implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idpersonal;

	private String nume;

	public Personal() {
	}

	/**
	 * Returns the id of a doctor
	 * */
	public int getIdpersonal() {
		return this.idpersonal;
	}

	/**
	 * set the id of a doctor
	 * @param idpersonal is the new id
	 * */
	public void setIdpersonal(int idpersonal) {
		this.idpersonal = idpersonal;
	}

	/**
	 * Returns the name of a doctor
	 * */
	public String getNume() {
		return this.nume;
	}

	/*
	 * Set the name of a doctor
	 * @param name is the new name
	 * */
	public void setNume(String name) {
		this.nume = name;
	}

}