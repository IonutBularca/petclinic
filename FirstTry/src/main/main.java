package main;


import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import model.Animal;
import model.Personal;
import model.Programare;
import util.DatabaseUtil;


public class main extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
	 try {
		BorderPane root = (BorderPane)  FXMLLoader.load(getClass().getResource("/ui/Login.fxml"));
		Scene scene = new Scene(root,800,800);
		primaryStage.setScene(scene);
		primaryStage.show();
	 } catch (Exception e) {
		e.printStackTrace();
	}
		
	}
	
	public static void main(String[] args) {
		launch(args);
	}
	
	
}
	
	
	/*
	
	
	
	
	
	
	public class main{
	
	
	
	
	@SuppressWarnings("deprecation")
	public static void main(String[] args) throws Exception {
	//	DatabaseUtil dbUtil = new DatabaseUtil();
	//	dbUtil.setUP();
	//	dbUtil.startTransaction();
		Scanner keyboard = new Scanner(System.in);
		Map<String,Integer> optionsMap = new HashMap<>();
		optionsMap.put("Create", 1);
		optionsMap.put("Read", 2);
		optionsMap.put("Update", 3);
		optionsMap.put("Delete", 4);
		optionsMap.put("SortedPrint", 5);
		optionsMap.put("Exit", 0);
		
		while(true) {
			DatabaseUtil dbUtil = new DatabaseUtil();
			dbUtil.setUP();
			dbUtil.startTransaction();
			
		
			
			System.out.println("Type Create , Read , Update , Delete , SortedPrint , Exit");
			
			System.out.print("Your option ");
			
	//		int opt = keyboard.nextInt();
			
			String optionString = keyboard.nextLine();
			while (optionString.trim().equals("")) {
				optionString = keyboard.nextLine();
			}
			int opt = optionsMap.get(optionString);
			
			if (opt == 0) {
					break;
			}
			if(opt == 1) {
				System.out.println("Press 1 for create record in Animal , 2 for Doctor, 3 for Appointment , 0 for exit");
				System.out.print("Your option ");
				int op2 = keyboard.nextInt();
				if(op2==1) {
					System.out.print("ID: ");
					int id = keyboard.nextInt();
					System.out.println("Name: ");
					String name = keyboard.next();
					Animal temp = new Animal();
					temp.setIdAnimal(id);
					temp.setName(name);
					dbUtil.saveAnimal(temp);
					dbUtil.commitTransaction();
				}
				if(op2==2) {
					System.out.print("ID: ");
					int id = keyboard.nextInt();
					System.out.println("Name: ");
					String name = keyboard.next();
					Personal temp = new Personal();
					temp.setIdpersonal(id);
					temp.setNume(name);
					dbUtil.savePersonal(temp);
					dbUtil.commitTransaction();
				}
				if(op2==3) {
					System.out.print("ID: ");
					int id = keyboard.nextInt();
					System.out.print("IDAnimal: ");
					int id2 = keyboard.nextInt();
					System.out.print("IDDoctor: ");
					int id3 = keyboard.nextInt();
					System.out.println("Date dd mm yyyy : ");
					int d,m,y;
					d=keyboard.nextInt();
					m=keyboard.nextInt();
					y=keyboard.nextInt();
					System.out.println("hh mm : ");
					int h,min;
					h=keyboard.nextInt();
					min=keyboard.nextInt();
					Date date = new Date(d,m,y,h,min);
				    Programare temp = new Programare();
				    temp.setDate(date);
				    temp.setIdanimal(id2);
				    temp.setIdpersonal(id3);
				    temp.setIdprogramare(id);
				    dbUtil.saveAppointment(temp);
				    dbUtil.commitTransaction();
				}
			}
			if(opt == 2) {
				System.out.println("Press 1 for read Animal , 2 for Doctor, 3 for Appointment , 0 for exit");
				System.out.print("Your option ");
				int op2 = keyboard.nextInt();
				if(op2==1) {
					dbUtil.printAllAnimalsFromDB();
				}
				if(op2==2) {
					dbUtil.printAllDoctorsFromDB();
				}
				if(op2==3) {
					dbUtil.printAllAppointmentsFromDB();
				}
				dbUtil.commitTransaction();
			}
			if(opt == 3) {
				System.out.println("Press 1 for update Animal , 2 for Doctor, 3 for Appointment , 0 for exit");
				System.out.print("Your option ");
				int opt2 = keyboard.nextInt();
				System.out.print("Enter the id: ");
				int id = keyboard.nextInt();
				if(opt2 == 1) {
					System.out.println("New name: ");
					String temp = keyboard.next();
					dbUtil.updateAnimal(id, temp);
				}
				if(opt2 == 2) {
					System.out.println("New name: ");
					String temp = keyboard.next();
					dbUtil.updateDoctor(id, temp);
				}
				if(opt2 == 3) {
					System.out.println("Press 1 for update AnimalID , 2 for DoctorID, 3 for Date , 0 for exit");
					System.out.print("Your option ");
					int opt3 = keyboard.nextInt();
					if(opt3==1) {
						System.out.println("New animalID");
						int temp = keyboard.nextInt();
						dbUtil.updateAppointmentAnimal(id, temp);
					}
					if(opt3==2) {
						System.out.println("New doctorID");
						int temp = keyboard.nextInt();
						dbUtil.updateAppointmentDoctor(id, temp);
					}
					if(opt3==3) {
						System.out.println("Date dd mm yyyy : ");
						int d,m,y;
						d=keyboard.nextInt();
						m=keyboard.nextInt();
						y=keyboard.nextInt();
						System.out.println("hh mm : ");
						int h,min;
						h=keyboard.nextInt();
						min=keyboard.nextInt();
						Date date = new Date(d,m,y,h,min);
					    dbUtil.updateAppointmentDate(id, date);
					}
				}
				dbUtil.commitTransaction();
			}
			if(opt == 4) {
				System.out.println("Press 1 for delete Animal , 2 for Doctor, 3 for Appointment , 0 for exit");
				System.out.print("Your option ");
				int opt2 = keyboard.nextInt();
				System.out.print("ID: ");
				int id = keyboard.nextInt();
				if(opt2==1) {
					dbUtil.deleteAnimal(id);
				}
				if(opt2==2) {
					dbUtil.deleteDoctor(id);
				}
				if(opt2==3) {
					dbUtil.deleteAppointment(id);
				}
				dbUtil.commitTransaction();
			}
			
		if(opt == 5) {
			dbUtil.sortPrint();
		}
		dbUtil.closeEntityManager();	
		}

	//	dbUtil.closeEntityManager();
		
		System.out.println("Have a nice day!");
	}
	
	
	
	}
	*/


