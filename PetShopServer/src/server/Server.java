package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import singleton.Singleton;

public class Server extends Thread {
	private Socket socket;
	
	public Server(Socket socket) {
		this.socket = socket;
	}
	
	@Override
	public void run() {
		try {
			BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintWriter output = new PrintWriter(socket.getOutputStream(),true);
			
			while(true) {
				String echoString = input.readLine();
				System.out.println("Receiver: "+echoString);
				if(echoString.equals("exit")) {
					break;
				}
				String user = Singleton.getInstance().getUserData();
				System.out.println(user);
			//nu stiu de ce dar nu ma lasa sa construiesc asta aici...
			
				if(echoString.equals(user))
				output.println("Accepted");
				
				
				
				else {
					output.println("Rejected");
				}
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			try {
				socket.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
}
