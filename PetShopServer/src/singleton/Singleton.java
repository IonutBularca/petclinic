package singleton;

public class Singleton {
	String usernameAndPassword = "Ion-1234";
	
	static Singleton instance = null;
	
	public static final Singleton getInstance() {
			
			if(instance == null) {
				instance = new Singleton();
			}
		
			return instance;
	}
	
	public String getUserData() {
			return usernameAndPassword;
	}
}
