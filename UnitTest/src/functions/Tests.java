package functions;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Tests {

	@Test
	 public void testApp()
    {
        assertTrue( true );
    }
	
	@Test
	 public void testFirst()
   {
		Integer i = 5;
		SomeFunctionsToTest.incrementByOne(i);
		System.out.println(i);
		//de ce i = 5? hmm...
		assertTrue( i.equals(6) );
   }
	
	@Test
	 public void testSecond()
   {
       assertTrue( "alabalaportocala".equals(SomeFunctionsToTest.getMessage()) );
   }
	
	
}
