package singleton;

public class Singleton {
	private String something;
	
	private static Singleton instance = null;
	
	public Singleton(){
			this.something = "Alabalaportocala";
	}
	
	public final static Singleton getInstance() {
		if(instance == null) {
				instance = new Singleton();
		}
		return instance;
			
	} 
	
	public final String getSomething() {
		return something;
	}
	public  final void setSomething(String s) {
		something = s;
	}
}
