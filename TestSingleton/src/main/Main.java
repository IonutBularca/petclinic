package main;

import singleton.Singleton;

public class Main {

	public static void main(String[] args) {
		System.out.println(Singleton.getInstance().getSomething());
		Singleton.getInstance().setSomething("ana are mere");
		System.out.println(Singleton.getInstance().getSomething());
		Singleton x = Singleton.getInstance();
		System.out.println(x.getSomething());
	}

}
